import React, {Component} from 'react';
import {
  MainWrap,
  YellowContent,
  YellowSection,
  Section,
  BackgroundYellow,
  HappyBirthdayText,
  Card,
  Chain,
  ChainCards,
  HeartIcon,
  PinkSection,
  BirthdayDate,
  BirthdayMessage,
  CardMessageIcon,
} from './styles';
import {ChainSvg, MessageIcon} from './svg';

const CardsData = {
  image: 'image',
  date: '01.02.1992',
  position: 1,
  message: 'hello',
};

class Main extends Component {
  state = {};
  render () {
    return (
      <MainWrap>
        <YellowSection>
          <BackgroundYellow>
            <Section first />
            <Section second />
            <Section third />
            <Section third />
          </BackgroundYellow>
          <YellowContent>
            <HappyBirthdayText>
              С днём рождения, Вика!
            </HappyBirthdayText>
            <ChainCards>
              <Chain><ChainSvg /></Chain>
              <Card>
                {/*<CardSvg />*/}
                <p>Text</p>
                <CardMessageIcon><MessageIcon /></CardMessageIcon>
              </Card>
            </ChainCards>
          </YellowContent>
        </YellowSection>
        <PinkSection>
          <BirthdayDate>
            01.06.18
          </BirthdayDate>
          <BirthdayMessage>
            Вика в 11-й раз празднует своё 18-летие. Мы желаем ей много красивых историй пережить, а главное — создать.
            И робко признаемся в любви️
          </BirthdayMessage>
          <HeartIcon>
            Icon
          </HeartIcon>
        </PinkSection>
      </MainWrap>
    );
  }
}

export default Main;
