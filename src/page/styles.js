import styled from 'styled-components';

export const MainWrap = styled.div`
`;

export const YellowSection = styled.section`
    display: flex;
    flex-flow: column nowrap;
    position: relative;
    height: 100%;
`;

export const PinkSection = styled.section`
    display: flex;
    flex-flow: column nowrap;
    align-items: center;
    padding: 130px 220px;
    background: linear-gradient(180deg, rgba(254, 73, 116, 0.86) 0%, #FE498A 100%);
`;

export const BirthdayDate = styled.p`
    margin: 0;
    margin-bottom: 62px;
`;

export const HeartIcon = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const BirthdayMessage = styled.p`
    margin: 0;
    margin-bottom: 62px;
`;

export const YellowContent = styled.div`
    display: flex;
    flex-flow: column nowrap;
    position: absolute;
    top: 100px;
    left: 50%;
    transform: translateX(-50%);
`;

export const Section = styled.div`
    height: 780px;
    position: relative;
    ${p => p.first && `
    background: linear-gradient(180deg, #FED545 0%, #FED545 30%);
    `}
    ${p => p.second && `
    background: linear-gradient(180deg, #FECD4E 0%, #FECD4E 0%);
    `}
    ${p => p.third && `
    background: linear-gradient(180deg, #FDCB48 0%, #FDCB48 0%);
    `}
    ${p => p.last && `
    background: linear-gradient(180deg, #FFCB44 0%, #FFCB44 0%);
    `}
`;

export const BackgroundYellow = styled.div`
    position: relative;
    display: flex;
    flex-flow: column nowrap;
    top: 0;
    left: 0;
    width: 100%;
`;

export const HappyBirthdayText = styled.h1`
    margin: 0;
    font-family: BSoftie;
    line-height: 52px;
    font-size: 55px;
    text-align: center;
    color: #fff;
`;

export const ChainCards = styled.div`
    position: relative;
`;

export const Chain = styled.div`
    position: absolute;
    left: 50%;
    top: 500px;
    transform: translateX(-50%);
`;

export const CardMessageIcon = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    bottom: -74px;
    left: 50%;
    transform: translateX(-50%);
`;

export const Card = styled.div`
    position: relative;
    width: 220px;
    height: 260px;
    background: #fff;
    position: absolute;
    left: 20%;
    top: 254px;
    transform: translateX(-50%) rotate(-12deg);
    rotate(-12deg)
`;
